From cc522ec3717d909370af6181c7859c62fa0167df Mon Sep 17 00:00:00 2001
From: Vojtech Trefny <vtrefny@redhat.com>
Date: Mon, 22 Feb 2021 15:40:56 +0100
Subject: [PATCH 1/2] fs: Allow using empty label for vfat with newest
 dosfstools

---
 src/plugins/fs/vfat.c | 11 +++++++++++
 1 file changed, 11 insertions(+)

diff --git a/src/plugins/fs/vfat.c b/src/plugins/fs/vfat.c
index ff0c35a3..ce13f147 100644
--- a/src/plugins/fs/vfat.c
+++ b/src/plugins/fs/vfat.c
@@ -232,10 +232,21 @@ gboolean bd_fs_vfat_repair (const gchar *device, const BDExtraArg **extra, GErro
  */
 gboolean bd_fs_vfat_set_label (const gchar *device, const gchar *label, GError **error) {
     const gchar *args[4] = {"fatlabel", device, label, NULL};
+    UtilDep dep = {"fatlabel", "4.2", "--version", "fatlabel\\s+([\\d\\.]+).+"};
+    gboolean new_vfat = FALSE;
 
     if (!check_deps (&avail_deps, DEPS_FATLABEL_MASK, deps, DEPS_LAST, &deps_check_lock, error))
         return FALSE;
 
+    if (!label || g_strcmp0 (label, "") == 0) {
+        /* fatlabel >= 4.2 refuses to set empty label */
+        new_vfat = bd_utils_check_util_version (dep.name, dep.version,
+                                                dep.ver_arg, dep.ver_regexp,
+                                                NULL);
+        if (new_vfat)
+            args[2] = "--reset";
+    }
+
     return bd_utils_exec_and_report_error (args, NULL, error);
 }
 

From c3c3583409c8ed8f99a840e0c70cc92ca1dd3c93 Mon Sep 17 00:00:00 2001
From: Vojtech Trefny <vtrefny@redhat.com>
Date: Tue, 27 Apr 2021 14:06:59 +0200
Subject: [PATCH 2/2] tests: Call fs_vfat_mkfs with "--mbr=n" extra option in
 tests

Without the option the newest dosfstools 4.2 will create a valid
MBR partition table with a simgle partition on the disk, see
dosfstools/dosfstools#95 for details.
---
 src/plugins/fs/vfat.c |  5 ++-
 tests/fs_test.py      | 76 +++++++++++++++++++++++++++++++++----------
 2 files changed, 62 insertions(+), 19 deletions(-)

diff --git a/src/plugins/fs/vfat.c b/src/plugins/fs/vfat.c
index ce13f147..6cb82537 100644
--- a/src/plugins/fs/vfat.c
+++ b/src/plugins/fs/vfat.c
@@ -234,6 +234,7 @@ gboolean bd_fs_vfat_set_label (const gchar *device, const gchar *label, GError *
     const gchar *args[4] = {"fatlabel", device, label, NULL};
     UtilDep dep = {"fatlabel", "4.2", "--version", "fatlabel\\s+([\\d\\.]+).+"};
     gboolean new_vfat = FALSE;
+    GError *loc_error = NULL;
 
     if (!check_deps (&avail_deps, DEPS_FATLABEL_MASK, deps, DEPS_LAST, &deps_check_lock, error))
         return FALSE;
@@ -242,9 +243,11 @@ gboolean bd_fs_vfat_set_label (const gchar *device, const gchar *label, GError *
         /* fatlabel >= 4.2 refuses to set empty label */
         new_vfat = bd_utils_check_util_version (dep.name, dep.version,
                                                 dep.ver_arg, dep.ver_regexp,
-                                                NULL);
+                                                &loc_error);
         if (new_vfat)
             args[2] = "--reset";
+        else
+            g_clear_error (&loc_error);
     }
 
     return bd_utils_exec_and_report_error (args, NULL, error);
diff --git a/tests/fs_test.py b/tests/fs_test.py
index 239cb47c..2233db4f 100644
--- a/tests/fs_test.py
+++ b/tests/fs_test.py
@@ -5,10 +5,13 @@
 import tempfile
 from contextlib import contextmanager
 import utils
-from utils import run, create_sparse_tempfile, mount, umount, TestTags, tag_test
+from utils import run, create_sparse_tempfile, mount, umount, TestTags, tag_test, run_command
+import re
 import six
 import overrides_hack
 
+from distutils.version import LooseVersion
+
 from gi.repository import BlockDev, GLib
 
 
@@ -29,9 +32,20 @@ def mounted(device, where, ro=False):
     yield
     umount(where)
 
+
+def _get_dosfstools_version():
+    _ret, out, _err = run_command("mkfs.vfat --help")
+    # mkfs.fat 4.1 (2017-01-24)
+    m = re.search(r"mkfs\.fat ([\d\.]+)", out)
+    if not m or len(m.groups()) != 1:
+        raise RuntimeError("Failed to determine dosfstools version from: %s" % out)
+    return LooseVersion(m.groups()[0])
+
+
 class FSTestCase(unittest.TestCase):
 
     requested_plugins = BlockDev.plugin_specs_from_names(("fs", "loop"))
+    _vfat_version = _get_dosfstools_version()
 
     @classmethod
     def setUpClass(cls):
@@ -66,6 +80,11 @@ def setUp(self):
 
         self.mount_dir = tempfile.mkdtemp(prefix="libblockdev.", suffix="ext4_test")
 
+        if self._vfat_version <= LooseVersion("4.1"):
+            self._mkfs_options = None
+        else:
+            self._mkfs_options = [BlockDev.ExtraArg.new("--mbr=n", "")]
+
     def _clean_up(self):
         try:
             utils.delete_lio_device(self.loop_dev)
@@ -120,7 +139,10 @@ def test_generic_wipe(self):
 
         # vfat has multiple signatures on the device so it allows us to test the
         # 'all' argument of fs_wipe()
-        ret = run("mkfs.vfat -I %s >/dev/null 2>&1" % self.loop_dev)
+        if self._vfat_version >= LooseVersion("4.2"):
+            ret = utils.run("mkfs.vfat -I %s >/dev/null 2>&1 --mbr=n" % self.loop_dev)
+        else:
+            ret = utils.run("mkfs.vfat -I %s >/dev/null 2>&1" % self.loop_dev)
         self.assertEqual(ret, 0)
 
         time.sleep(0.5)
@@ -142,7 +164,10 @@ def test_generic_wipe(self):
         self.assertEqual(fs_type, b"")
 
         # now do the wipe all in a one step
-        ret = run("mkfs.vfat -I %s >/dev/null 2>&1" % self.loop_dev)
+        if self._vfat_version >= LooseVersion("4.2"):
+            ret = utils.run("mkfs.vfat -I %s >/dev/null 2>&1 --mbr=n" % self.loop_dev)
+        else:
+            ret = utils.run("mkfs.vfat -I %s >/dev/null 2>&1" % self.loop_dev)
         self.assertEqual(ret, 0)
 
         succ = BlockDev.fs_wipe(self.loop_dev, True)
@@ -197,7 +222,10 @@ def test_clean(self):
 
         # vfat has multiple signatures on the device so it allows us to test
         # that clean removes all signatures
-        ret = run("mkfs.vfat -I %s >/dev/null 2>&1" % self.loop_dev)
+        if self._vfat_version >= LooseVersion("4.2"):
+            ret = utils.run("mkfs.vfat -I %s >/dev/null 2>&1 --mbr=n" % self.loop_dev)
+        else:
+            ret = utils.run("mkfs.vfat -I %s >/dev/null 2>&1" % self.loop_dev)
         self.assertEqual(ret, 0)
 
         time.sleep(0.5)
@@ -744,9 +772,9 @@ def test_vfat_mkfs(self):
         """Verify that it is possible to create a new vfat file system"""
 
         with self.assertRaises(GLib.GError):
-            BlockDev.fs_vfat_mkfs("/non/existing/device", None)
+            BlockDev.fs_vfat_mkfs("/non/existing/device", self._mkfs_options)
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         # just try if we can mount the file system
@@ -764,7 +792,10 @@ def test_vfat_mkfs_with_label(self):
         """Verify that it is possible to create an vfat file system with label"""
 
         ea = BlockDev.ExtraArg.new("-n", "TEST_LABEL")
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, [ea])
+        if self._mkfs_options:
+            succ = BlockDev.fs_vfat_mkfs(self.loop_dev, [ea] + self._mkfs_options)
+        else:
+            succ = BlockDev.fs_vfat_mkfs(self.loop_dev, [ea])
         self.assertTrue(succ)
 
         fi = BlockDev.fs_vfat_get_info(self.loop_dev)
@@ -775,7 +806,7 @@ class VfatTestWipe(FSTestCase):
     def test_vfat_wipe(self):
         """Verify that it is possible to wipe an vfat file system"""
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         succ = BlockDev.fs_vfat_wipe(self.loop_dev)
@@ -805,7 +836,7 @@ class VfatTestCheck(FSTestCase):
     def test_vfat_check(self):
         """Verify that it is possible to check an vfat file system"""
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         succ = BlockDev.fs_vfat_check(self.loop_dev, None)
@@ -818,7 +849,7 @@ class VfatTestRepair(FSTestCase):
     def test_vfat_repair(self):
         """Verify that it is possible to repair an vfat file system"""
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         succ = BlockDev.fs_vfat_repair(self.loop_dev, None)
@@ -828,7 +859,7 @@ class VfatGetInfo(FSTestCase):
     def test_vfat_get_info(self):
         """Verify that it is possible to get info about an vfat file system"""
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         fi = BlockDev.fs_vfat_get_info(self.loop_dev)
@@ -841,7 +872,7 @@ class VfatSetLabel(FSTestCase):
     def test_vfat_set_label(self):
         """Verify that it is possible to set label of an vfat file system"""
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         fi = BlockDev.fs_vfat_get_info(self.loop_dev)
@@ -870,7 +901,7 @@ class VfatResize(FSTestCase):
     def test_vfat_resize(self):
         """Verify that it is possible to resize an vfat file system"""
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         # shrink
@@ -999,7 +1030,7 @@ def _remove_user(self):
     def test_mount(self):
         """ Test basic mounting and unmounting """
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         tmp = tempfile.mkdtemp(prefix="libblockdev.", suffix="mount_test")
@@ -1104,7 +1135,7 @@ def test_mount_fstab(self):
         fstab = utils.read_file("/etc/fstab")
         self.addCleanup(utils.write_file, "/etc/fstab", fstab)
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         tmp = tempfile.mkdtemp(prefix="libblockdev.", suffix="mount_fstab_test")
@@ -1139,7 +1170,7 @@ def test_mount_fstab_user(self):
         fstab = utils.read_file("/etc/fstab")
         self.addCleanup(utils.write_file, "/etc/fstab", fstab)
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         tmp = tempfile.mkdtemp(prefix="libblockdev.", suffix="mount_fstab_user_test")
@@ -1423,7 +1454,16 @@ def expected_size(fi):
     @tag_test(TestTags.UNSTABLE)
     def test_vfat_generic_resize(self):
         """Test generic resize function with a vfat file system"""
-        self._test_generic_resize(mkfs_function=BlockDev.fs_vfat_mkfs)
+        def mkfs_vfat(device, options=None):
+            if self._vfat_version >= LooseVersion("4.2"):
+                if options:
+                    return BlockDev.fs_vfat_mkfs(device, options + [BlockDev.ExtraArg.new("--mbr=n", "")])
+                else:
+                    return BlockDev.fs_vfat_mkfs(device, [BlockDev.ExtraArg.new("--mbr=n", "")])
+            else:
+                return BlockDev.fs_vfat_mkfs(device, options)
+
+        self._test_generic_resize(mkfs_function=mkfs_vfat)
 
     def _destroy_lvm(self):
         run("vgremove --yes libbd_fs_tests >/dev/null 2>&1")
@@ -1539,7 +1579,7 @@ def test_freeze_xfs(self):
     def test_freeze_vfat(self):
         """ Test basic freezing and un-freezing with FAT """
 
-        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, None)
+        succ = BlockDev.fs_vfat_mkfs(self.loop_dev, self._mkfs_options)
         self.assertTrue(succ)
 
         tmp = tempfile.mkdtemp(prefix="libblockdev.", suffix="freeze_test")
